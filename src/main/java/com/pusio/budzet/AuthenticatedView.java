package com.pusio.budzet;

import com.pusio.budzet.DAOimpl.UserDaoImpl;
import com.pusio.budzet.entity.Panda;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public abstract class AuthenticatedView extends VerticalLayout implements View {
	protected static Panda actualUser;
	
	@Override
	public void enter(ViewChangeEvent event) {
		//getUI().getNavigator().navigateTo(MyUI.NOT_REGISTERED);
		
		if(VaadinSession.getCurrent().getAttribute("user") == null ) {	
			Notification.show("LOGIN");
			getUI().getNavigator().navigateTo(MyUI.NOT_REGISTERED);
		}
		else {
			actualUser = new UserDaoImpl().getById((Long) VaadinSession.getCurrent().getAttribute("user"));
			Notification.show("ACTUAL USER AUTH: " + (Long) VaadinSession.getCurrent().getAttribute("user") + "|" + actualUser.getLogin());
		}
	}

	public static Panda getActualUser() {
		return actualUser;
	}
	
}
