package com.pusio.budzet.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class OutcomesCategory {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String name;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "category")
	private List<Outcomes> outcomesList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Outcomes> getOutcomesList() {
		return outcomesList;
	}

	public void setOutcomesList(List<Outcomes> outcomesList) {
		this.outcomesList = outcomesList;
	}

	public OutcomesCategory(String name) {
		super();
		this.name = name;
	}

	public OutcomesCategory() {
		super();
	}

	@Override
	public String toString() {
		return name;
	}

}
