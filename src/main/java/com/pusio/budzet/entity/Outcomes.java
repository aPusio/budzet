package com.pusio.budzet.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Outcomes implements PandaTransaction {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private double amount;
	private LocalDateTime date;

	@ManyToOne
	private Panda owner;

	@ManyToOne
	private OutcomesCategory category;

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Panda getOwner() {
		return owner;
	}

	public void setOwner(Panda owner) {
		this.owner = owner;
	}

	public OutcomesCategory getCategory() {
		return category;
	}

	public void setCategory(OutcomesCategory category) {
		this.category = category;
	}

	@Override
	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public Outcomes() {
		super();
	}

	public Outcomes(double amount, LocalDateTime date, Panda owner, OutcomesCategory category) {
		super();
		this.amount = amount;
		this.date = date;
		this.owner = owner;
		this.category = category;
	}

}
