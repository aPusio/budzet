package com.pusio.budzet.entity;

import java.time.LocalDateTime;

public interface PandaTransaction {
	public LocalDateTime getDate();

	public double getAmount();

	public Long getId();
}
