package com.pusio.budzet.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class IncomesCategory {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String name;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "category")
	private List<Incomes> incomesList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Incomes> getIncomesList() {
		return incomesList;
	}

	public void setIncomesList(List<Incomes> incomesList) {
		this.incomesList = incomesList;
	}

	public IncomesCategory(String name) {
		super();
		this.name = name;
	}

	public IncomesCategory() {
		super();
	}
	@Override
	public String toString() {
		return name;
	}
}
