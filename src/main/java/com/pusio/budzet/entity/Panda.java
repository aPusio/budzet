package com.pusio.budzet.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Panda {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String login;
	private String password;
	private String email;

	@OneToMany(mappedBy = "owner")
	private List<Incomes> incomesList;

	@OneToMany(mappedBy = "owner")
	private List<Outcomes> outcomesList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;

	}

	public List<Incomes> getIncomesList() {
		return incomesList;
	}

	public void setIncomesList(List<Incomes> incomesList) {
		this.incomesList = incomesList;
	}

	public List<Outcomes> getOutcomesList() {
		return outcomesList;
	}

	public void setOutcomesList(List<Outcomes> outcomesList) {
		this.outcomesList = outcomesList;
	}

	public Panda() {
		super();
	}

	public Panda(String login, String password, String email) {
		super();
		this.login = login;
		this.password = password;
		this.email = email;
	}

	@Override
	public String toString() {
		return login;
	}

}
