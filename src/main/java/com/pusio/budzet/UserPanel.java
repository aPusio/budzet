package com.pusio.budzet;

import com.pusio.budzet.components.PieChartIncomes;
import com.pusio.budzet.components.PieChartOutcomes;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.HorizontalLayout;

@SuppressWarnings("serial")
public class UserPanel extends AuthenticatedView {
	public UserPanel() {
		HorizontalLayout hLayout = new HorizontalLayout();
		hLayout.addComponentsAndExpand(new PieChartIncomes(), new PieChartOutcomes());
		this.addComponentsAndExpand(hLayout);
	}

	@Override
	public void enter(ViewChangeEvent event) {
		super.enter(event);
		this.removeAllComponents();
		HorizontalLayout hLayout = new HorizontalLayout();
		hLayout.addComponentsAndExpand(new PieChartIncomes(), new PieChartOutcomes());
		this.addComponentsAndExpand(hLayout);
	}
}
