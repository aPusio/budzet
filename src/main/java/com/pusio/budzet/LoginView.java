package com.pusio.budzet;


import com.pusio.budzet.DAOimpl.UserDaoImpl;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;

import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class LoginView extends VerticalLayout implements View {
	private TextField username = new TextField("Username");
	private TextField password = new TextField("Password");
	public LoginView() {
		setSizeFull();
		setSpacing(true);
		
		Label label = new Label("Enter your information below to log in.");
		
		addComponent(label);
		addComponent(username);
		addComponent(password);
		addComponent(loginButton());
		addComponent(registerButton());
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		Notification.show("Welcome! Please log in.");
	}
	private Button registerButton() {
		Button button = new Button("Register", new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				getUI().getNavigator().navigateTo(MyUI.NOT_REGISTERED);
				//NavigationDrawerBuilder.get().build().getUI().getNavigator().navigateTo(MyUI.NOT_REGISTERED);
			}
		});
		return button;
	}
	private Button loginButton() {
		Button button = new Button("Log In", new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				
				Long userId = new UserDaoImpl().getIDbyLoginAndPassword(username.getValue(), password.getValue());
				
				if(userId > 0) {
					VaadinSession.getCurrent().setAttribute("user", userId);
					MyUI.loginUser(new UserDaoImpl().getById(userId).getLogin());
					
				}
				
				getUI().getNavigator().navigateTo(MyUI.USER_PANEL);
				
			}
		});
		return button;
	}

}
