package com.pusio.budzet;

import java.util.List;
import com.pusio.budzet.DAOimpl.OutcomesDaoImpl;
import com.pusio.budzet.entity.Outcomes;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;



@SuppressWarnings("serial")
public class TransactionsView extends AuthenticatedView {

	Grid<Outcomes> gridOfOutcomes;
	
	public TransactionsView() {		
		gridOfOutcomes = new Grid<Outcomes>();
		List <Outcomes> list = new OutcomesDaoImpl().getAll();
		
		gridOfOutcomes.setSelectionMode(SelectionMode.NONE); 
        TextField nameEditor = new TextField();
       
		addComponent(
				new HorizontalLayout( new Label("Wydatki"), gridOfOutcomes ));
		
		

	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		super.enter(event);
		gridOfOutcomes.setItems(new OutcomesDaoImpl().getAll());
	}

}
