package com.pusio.budzet.DAOimpl;

import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.pusio.budzet.DAO.OutcomesDAO;
import com.pusio.budzet.entity.Outcomes;
import com.pusio.budzet.entity.OutcomesCategory;
import com.pusio.budzet.hibernate.HibernateUtil;

public class OutcomesDaoImpl implements OutcomesDAO {
	private final Logger logger = Logger.getLogger(getClass().getName());
	private SessionFactory sessionFactory;

	// for dev
	public OutcomesDaoImpl() {
		sessionFactory = HibernateUtil.getSessionFactory();
	}

	@Override
	public void create(Outcomes entity) {
		// entity.setAmount(entity.getAmount()* -1);

		// TODO Auto-generated method stub
		Transaction tx = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.persist(entity);
			tx.commit();
		} catch (HibernateException ex) {
			logger.info("Create error: " + ex.getLocalizedMessage());
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public void delete(Outcomes entity) {
		// TODO Auto-generated method stub
		Transaction tx = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.delete(entity);
			tx.commit();
		} catch (Exception ex) {
			logger.info("Delete error: " + ex.getLocalizedMessage());
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	@Override
	public List<Outcomes> getAll() {
		try (Session session = sessionFactory.openSession()) {
			return session.createQuery("from Outcomes").list();

		} catch (HibernateException ex) {
			logger.info("Select all error: " + ex.getLocalizedMessage());
			return null;
		}
	}

	@Override
	public Outcomes getById(Long id) {
		try (Session session = sessionFactory.openSession()) {
			Outcomes user = session.get(Outcomes.class, id);
			return user;
		} catch (HibernateException ex) {
			logger.info("Get by id error: " + ex.getLocalizedMessage());
			return null;
		}
	}

	@Override
	public void update(Outcomes entity) {
		Transaction tx = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.update(entity);
			tx.commit();
		} catch (HibernateException ex) {
			logger.info("Update error: " + ex.getLocalizedMessage());
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public HashMap<String, Double> getCategoryData() {
		HashMap<String, Double> result = new HashMap<>();
		try (Session session = sessionFactory.openSession()) {

			Query q = session.createQuery("select o.amount, categ.name " + "from Outcomes o join o.category categ");

			List<Object[]> resultList = (List<Object[]>) q.list();
			for (Object[] singleResult : resultList) {
				Double amount = (Double) singleResult[0];
				String catName = (String) singleResult[1];
				result.put(catName, amount);
			}

			return result;
		} catch (HibernateException ex) {
			logger.info("Select all error: " + ex.getLocalizedMessage());
			return null;
		}
	}
}
