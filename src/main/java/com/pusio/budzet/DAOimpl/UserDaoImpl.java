package com.pusio.budzet.DAOimpl;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.pusio.budzet.DAO.UserDAO;
import com.pusio.budzet.entity.Incomes;
import com.pusio.budzet.entity.Panda;

import com.pusio.budzet.hibernate.HibernateUtil;

public class UserDaoImpl implements UserDAO {
	private final Logger logger = Logger.getLogger(getClass().getName());
	private SessionFactory sessionFactory;

	// for dev
	public UserDaoImpl() {
		sessionFactory = HibernateUtil.getSessionFactory();
	}

	@Override
	public void create(Panda entity) {
		// TODO Auto-generated method stub
		Transaction tx = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.persist(entity);
			tx.commit();
		} catch (HibernateException ex) {
			logger.info("Create error: " + ex.getLocalizedMessage());
			if (tx != null) { tx.rollback(); }
		} finally {
			if (session != null) { session.close(); }
		}
	}

	@Override
	public void delete(Panda entity) {
		// TODO Auto-generated method stub
		Transaction tx = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.delete(entity);
			tx.commit();
		} catch (Exception ex) {
			logger.info("Delete error: " + ex.getLocalizedMessage());
			if (tx != null) { tx.rollback(); }
		} finally {
			if (session != null) { session.close(); }
		}

	}

	@Override
	public List<Panda> getAll() {
		try (Session session = sessionFactory.openSession()) {
			return session.createQuery("from Panda").list();

		} catch (HibernateException ex) {
			logger.info("Select all error: " + ex.getLocalizedMessage());
			return null;
		}
	}

	@Override
	public Panda getById(Long id) {
		try (Session session = sessionFactory.openSession()) {
			Panda user = session.get(Panda.class, id);
			return user;
		} catch (HibernateException ex) {
			logger.info("Get by id error: " + ex.getLocalizedMessage());
			return null;
		}
	}
	
	//znalezc sesje 
	//wrzucic usera
	//from user where login = $ and pas = $
	
	@Override
	public void update(Panda entity) {
		Transaction tx = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.update(entity);
			tx.commit();
		} catch (HibernateException ex) {
			logger.info("Update error: " + ex.getLocalizedMessage());
			if (tx != null) { tx.rollback(); }
		} finally {
			if (session != null) { session.close(); }
		}
	}
	
	public Long getIDbyLoginAndPassword(String login, String password) {
		try (Session session = sessionFactory.openSession()) {
			List<Panda> founded = session.createQuery("from Panda where login = '" + login + "' and password = '" + password + "'").list();
		
			return founded.size() > 0 ? founded.get(0).getId() : -1l;
		} catch (HibernateException ex) {
			logger.info("Select all error: " + ex.getLocalizedMessage());
			return -1l;
		}
		
		//return -1l;
	}
	public Double getTotalAmount(Long userID) {
		try (Session session = sessionFactory.openSession()) {
			
//			String hql = "SELECT sum(o.totalAmount) as total, o.createdAt from Order o"
//		             + " where o.createdAt < :limit"
//		             + " and o.doctor.id = 193"
//		             + " group by o.createdAt";
			
			
			String hql = "select sum(inc.amount) from Incomes inc where inc.owner.id = " + userID;
			List<Double> founded = session.createQuery(hql).list();
		
			return founded.get(0) != null ? founded.get(0) : 0d;
			//return 20;
		} catch (HibernateException ex) {
			logger.info("Select all error: " + ex.getLocalizedMessage());
			return 0d;
		}
	}
	
//	public 
}
