package com.pusio.budzet.DAOimpl;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.pusio.budzet.DAO.OutcomesCategoryDAO;
import com.pusio.budzet.entity.OutcomesCategory;
import com.pusio.budzet.hibernate.HibernateUtil;

public class OutcomesCategoryDaoImpl implements OutcomesCategoryDAO {
	private final Logger logger = Logger.getLogger(getClass().getName());
	private SessionFactory sessionFactory;

	// for dev
	public OutcomesCategoryDaoImpl() {
		sessionFactory = HibernateUtil.getSessionFactory();
	}

	@Override
	public void create(OutcomesCategory entity) {
		// TODO Auto-generated method stub
		Transaction tx = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.persist(entity);
			tx.commit();
		} catch (HibernateException ex) {
			logger.info("Create error: " + ex.getLocalizedMessage());
			if (tx != null) { tx.rollback(); }
		} finally {
			if (session != null) { session.close(); }
		}
	}

	@Override
	public void delete(OutcomesCategory entity) {
		// TODO Auto-generated method stub
		Transaction tx = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.delete(entity);
			tx.commit();
		} catch (Exception ex) {
			logger.info("Delete error: " + ex.getLocalizedMessage());
			if (tx != null) { tx.rollback(); }
		} finally {
			if (session != null) { session.close(); }
		}

	}

	@Override
	public List<OutcomesCategory> getAll() {
		try (Session session = sessionFactory.openSession()) {
			return session.createQuery("from OutcomesCategory").list();

		} catch (HibernateException ex) {
			logger.info("Select all error: " + ex.getLocalizedMessage());
			return null;
		}
	}

	@Override
	public OutcomesCategory getById(Long id) {
		try (Session session = sessionFactory.openSession()) {
			OutcomesCategory user = session.get(OutcomesCategory.class, id);
			return user;
		} catch (HibernateException ex) {
			logger.info("Get by id error: " + ex.getLocalizedMessage());
			return null;
		}
	}

	@Override
	public void update(OutcomesCategory entity) {
		Transaction tx = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.update(entity);
			tx.commit();
		} catch (HibernateException ex) {
			logger.info("Update error: " + ex.getLocalizedMessage());
			if (tx != null) { tx.rollback(); }
		} finally {
			if (session != null) { session.close(); }
		}
	}
}
