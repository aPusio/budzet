package com.pusio.budzet;

import java.time.LocalDateTime;

import javax.servlet.annotation.WebServlet;

import com.github.appreciated.builder.DrawerVariant;
import com.github.appreciated.builder.NavigationDrawerBuilder;
import com.github.appreciated.layout.drawer.AbstractNavigationDrawer;
import com.pusio.budzet.DAOimpl.IncomesCategoryDaoImpl;
import com.pusio.budzet.DAOimpl.IncomesDaoImpl;
import com.pusio.budzet.DAOimpl.OutcomesCategoryDaoImpl;
import com.pusio.budzet.DAOimpl.OutcomesDaoImpl;
import com.pusio.budzet.DAOimpl.UserDaoImpl;
import com.pusio.budzet.entity.Incomes;
import com.pusio.budzet.entity.IncomesCategory;
import com.pusio.budzet.entity.Outcomes;
import com.pusio.budzet.entity.OutcomesCategory;
import com.pusio.budzet.entity.Panda;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;

/**
 * This UI is the application entry point. A UI may either represent a browser
 * window (or tab) or some part of a html page where a Vaadin application is
 * embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is
 * intended to be overridden to add component to the user interface and
 * initialize non-component functionality.
 */

@Theme("mytheme")
// @SuppressWarnings("serial")
public class MyUI extends UI {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public Navigator navigator;
	public static final String LOGIN = "Login";
	public static final String LOGOFF = "Logoff";
	public static final String REGISTER = "Register";
	public static final String VIEW_USERS = "View Users";

	public static final String ADD_INCOMES = "Add Incomes";
	public static final String ADD_OUTCOMES = "Add Outcomes";
	public static final String TRANSACTIONS = "Transaction List";
	public static final String NOT_REGISTERED = "Not Registered User";
	public static final String USER_PANEL = "User Panel";
	private static AbstractNavigationDrawer abstractNavigationDrawer;

	private static Button getBorderlessButtonWithIcon(VaadinIcons icon) {
		Button button = new Button(icon);
		button.addStyleNames(ValoTheme.BUTTON_BORDERLESS);
		return button;
	}

	public static AbstractNavigationDrawer getAbstractNavigationDrawer() {
		return abstractNavigationDrawer;
	}

	public static void setAbstractNavigationDrawer(AbstractNavigationDrawer abstractNavigationDrawer) {
		MyUI.abstractNavigationDrawer = abstractNavigationDrawer;
	}

	@Override
	protected void init(VaadinRequest vaadinRequest) {
		dummyAddToDB();

		abstractNavigationDrawer = getNavigationView();
		setContent(abstractNavigationDrawer);

		this.getUI().getNavigator().addView(NOT_REGISTERED, NotRegisteredUserView.class);
		//addMenuOptionsToLoggedUser();
	}

	private static void dummyAddToDB() {
		Panda wiesiek = new Panda("Wiesiek222", "6666", "wiesiek222@wp.pl");
		Panda mietek = new Panda("Mietek222", "6666", "Mietek222@wp.pl");
		Panda zenek = new Panda("Zenek222", "6666", "Zenek222@wp.pl");
		Panda felek = new Panda("Felek222", "6666", "Felek222@wp.pl");
		Panda aaa = new Panda("aaa", "aaa", "aaa");

		new UserDaoImpl().create(wiesiek);
		new UserDaoImpl().create(mietek);
		new UserDaoImpl().create(zenek);
		new UserDaoImpl().create(felek);
		new UserDaoImpl().create(aaa);

		IncomesCategory cat1 = new IncomesCategory("Praca");
		IncomesCategory cat2 = new IncomesCategory("Zlecenie");
		IncomesCategory cat3 = new IncomesCategory("Inne");
		IncomesCategory cat4 = new IncomesCategory("Stypendium");
		IncomesCategory cat5 = new IncomesCategory("Prezenty");

		new IncomesCategoryDaoImpl().create(cat1);
		new IncomesCategoryDaoImpl().create(cat2);
		new IncomesCategoryDaoImpl().create(cat3);
		new IncomesCategoryDaoImpl().create(cat4);
		new IncomesCategoryDaoImpl().create(cat5);

		OutcomesCategory cat11 = new OutcomesCategory("Czynsz");
		OutcomesCategory cat22 = new OutcomesCategory("Woda");
		OutcomesCategory cat33 = new OutcomesCategory("Gaz");
		OutcomesCategory cat44 = new OutcomesCategory("Prezenty");
		OutcomesCategory cat55 = new OutcomesCategory("Wycieczki");

		new OutcomesCategoryDaoImpl().create(cat11);
		new OutcomesCategoryDaoImpl().create(cat22);
		new OutcomesCategoryDaoImpl().create(cat33);
		new OutcomesCategoryDaoImpl().create(cat44);
		new OutcomesCategoryDaoImpl().create(cat55);

		Incomes incomes = new Incomes(50.0, LocalDateTime.now(), aaa, cat1);

		new IncomesDaoImpl().create(incomes);

		Outcomes outcome1 = new Outcomes(10.0, LocalDateTime.now(), aaa, cat11);
		Outcomes outcome2 = new Outcomes(5.0, LocalDateTime.now(), aaa, cat22);
		Outcomes outcome3 = new Outcomes(6.0, LocalDateTime.now(), aaa, cat33);
		Outcomes outcome4 = new Outcomes(20.0, LocalDateTime.now(), aaa, cat11);

		new OutcomesDaoImpl().create(outcome1);
		new OutcomesDaoImpl().create(outcome2);
		new OutcomesDaoImpl().create(outcome3);
		new OutcomesDaoImpl().create(outcome4);
	}

	public AbstractNavigationDrawer getNavigationView() {
		return NavigationDrawerBuilder.get()
				.withVariant(DrawerVariant.LEFT_RESPONSIVE)
				.withTitle("BUDZET")
				//.withAppBarElement(getBorderlessButtonWithIcon(VaadinIcons.ELLIPSIS_DOTS_V))
				.withDefaultNavigationView(Index.class).withSection("User panel")
				.withNavigationElement(LOGIN, VaadinIcons.USER, LoginView.class)
				.withNavigationElement(REGISTER, VaadinIcons.USER_CARD, RegisterView.class)
				.withNavigationElement(VIEW_USERS, VaadinIcons.USERS, AllUsersView.class)
				.withSection("Transactions")
				.withNavigationElement(ADD_INCOMES, VaadinIcons.PLUS, new AddIncomesView())
				.withNavigationElement(ADD_OUTCOMES, VaadinIcons.MINUS, new AddOutcomesView())
				.withNavigationElement(TRANSACTIONS, VaadinIcons.LIST, new TransactionsView())
				.withNavigationElement(USER_PANEL, VaadinIcons.HOME, new UserPanel()).build();
	}

	public static void loginUser(String username) {
		Button userButton = getBorderlessButtonWithIcon(VaadinIcons.USER);
		userButton.setCaption("Witam, " + username);
		
		Button logOff = getBorderlessButtonWithIcon(VaadinIcons.POWER_OFF);
		logOff.addClickListener(click -> {
			VaadinSession.getCurrent().setAttribute("user", null);
			Label header = new Label("BUDZET");
			abstractNavigationDrawer.getAppBar().removeAllComponents();
			abstractNavigationDrawer.getAppBar().addComponent(header);
			
			abstractNavigationDrawer.getAppBar().setComponentAlignment(header, Alignment.MIDDLE_LEFT);
			MyUI.getCurrent().getNavigator().navigateTo("");
		});
		
		
		abstractNavigationDrawer.getAppBar().addComponent(userButton);
		abstractNavigationDrawer.getAppBar().setComponentAlignment(userButton, Alignment.MIDDLE_RIGHT);

		abstractNavigationDrawer.getAppBar().addComponent(logOff);
		abstractNavigationDrawer.getAppBar().setComponentAlignment(logOff, Alignment.MIDDLE_RIGHT);
	}

	@WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
	@VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
	@SuppressWarnings("serial")
	public static class MyUIServlet extends VaadinServlet {
	}
}
