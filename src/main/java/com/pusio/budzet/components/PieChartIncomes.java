package com.pusio.budzet.components;

import java.util.HashMap;

import com.pusio.budzet.DAOimpl.IncomesDaoImpl;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

import at.downdrown.vaadinaddons.highchartsapi.Colors;
import at.downdrown.vaadinaddons.highchartsapi.HighChart;
import at.downdrown.vaadinaddons.highchartsapi.HighChartFactory;
import at.downdrown.vaadinaddons.highchartsapi.exceptions.HighChartsException;
import at.downdrown.vaadinaddons.highchartsapi.exceptions.NoChartTypeException;
import at.downdrown.vaadinaddons.highchartsapi.model.ChartConfiguration;
import at.downdrown.vaadinaddons.highchartsapi.model.ChartType;
import at.downdrown.vaadinaddons.highchartsapi.model.data.PieChartData;
import at.downdrown.vaadinaddons.highchartsapi.model.series.PieChartSeries;

@SuppressWarnings("serial")
public class PieChartIncomes extends Panel {
	HighChart pieChart;
	PieChartSeries pieIncomes;
	VerticalLayout layout;
	ChartConfiguration pieConfiguration;
	
	public PieChartIncomes() {
		layout = new VerticalLayout();
		
		// *** PIE ***
		pieConfiguration = new ChartConfiguration();
		pieConfiguration.setTitle("Twoje dochody");
		pieConfiguration.setChartType(ChartType.PIE);
		pieConfiguration.setBackgroundColor(Colors.WHITE);
		
		HashMap<String, Double> dataFromDB = new IncomesDaoImpl().getCategoryData();
		
		//ADD title
		pieIncomes = new PieChartSeries("Dochody");
		
		//APEND DATA
		for(java.util.Map.Entry<String, Double> ent : dataFromDB.entrySet()) {
			pieIncomes.getData().add(new PieChartData(ent.getKey(), ent.getValue()));
		}
		
		
//		PieChartData bananas = new PieChartData("Bananas", 33.2);
//		PieChartData melons = new PieChartData("Melons", 6.21);
//		PieChartData apples = new PieChartData("Apples", 3.44);
//
//		pieFruits.getData().add(bananas);
//		pieFruits.getData().add(melons);
//		pieFruits.getData().add(apples);

		pieConfiguration.getSeriesList().add(pieIncomes);
		try {
			pieChart = HighChartFactory.renderChart(pieConfiguration);
			pieChart.setHeight(80, Unit.PERCENTAGE);
			pieChart.setWidth(80, Unit.PERCENTAGE);
			System.out.println("PieChart Script : " + pieConfiguration.getHighChartValue());
			layout.addComponent(pieChart);
			layout.setComponentAlignment(pieChart, Alignment.MIDDLE_CENTER);
		} catch (NoChartTypeException e) {
			e.printStackTrace();
		} catch (HighChartsException e) {
			e.printStackTrace();
		}
		setContent(layout);
		
	}
	
	public void update() {
		pieIncomes.clearData();
		HashMap<String, Double> dataFromDB = new IncomesDaoImpl().getCategoryData();
		
		//APEND DATA
		for(java.util.Map.Entry<String, Double> ent : dataFromDB.entrySet()) {
			pieIncomes.getData().add(new PieChartData(ent.getKey(), ent.getValue()));
		}

		try {
			pieChart = HighChartFactory.renderChart(pieConfiguration);
		} catch (HighChartsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		setContent(layout);
	}
}
