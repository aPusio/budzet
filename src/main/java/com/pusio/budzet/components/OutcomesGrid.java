package com.pusio.budzet.components;

import com.pusio.budzet.DAOimpl.OutcomesDaoImpl;
import com.pusio.budzet.entity.Outcomes;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class OutcomesGrid extends VerticalLayout {
	Grid<Outcomes> gridOfOutcomes;

	public void initGrid() {
		
		gridOfOutcomes = new Grid<Outcomes>();
		gridOfOutcomes.setCaption("Kliknij dwukrotnie aby edytować");
		gridOfOutcomes.setSizeFull();
		
		gridOfOutcomes.setItems(new OutcomesDaoImpl().getAll());
		gridOfOutcomes.addColumn(Outcomes::getAmount).setCaption("AMOUNT");
		gridOfOutcomes.addColumn(Outcomes::getCategory).setCaption("Category");
		gridOfOutcomes.getEditor().setEnabled(true);

	}

	public void updateGrid() {
		gridOfOutcomes.setItems(new OutcomesDaoImpl().getAll());
	}

	public OutcomesGrid() {
		initGrid();
		addComponent(gridOfOutcomes);
	}

}
