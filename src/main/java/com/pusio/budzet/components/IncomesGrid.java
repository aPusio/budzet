package com.pusio.budzet.components;

import com.pusio.budzet.DAOimpl.IncomesDaoImpl;
import com.pusio.budzet.entity.Incomes;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class IncomesGrid extends VerticalLayout{
    Grid<Incomes> gridOfIncomes;
    
    public void initGrid() {
    	gridOfIncomes = new Grid<Incomes>();
		gridOfIncomes.setItems(new IncomesDaoImpl().getAllforOneUser(5l));
		gridOfIncomes.addColumn(Incomes::getAmount).setCaption("AMOUNT");
		gridOfIncomes.addColumn(Incomes::getCategory).setCaption("Category");
    }
    public void updateGrid() {
    	//gridOfIncomes.setItems(new IncomesDaoImpl().getAll());
    	gridOfIncomes.setItems(new IncomesDaoImpl().getAllforOneUser(5l));
    }
    
    public IncomesGrid() {
    	initGrid();
    	addComponent(gridOfIncomes);
	}
}
