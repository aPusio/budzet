package com.pusio.budzet;


import java.time.LocalDateTime;

import com.pusio.budzet.DAOimpl.OutcomesCategoryDaoImpl;
import com.pusio.budzet.DAOimpl.OutcomesDaoImpl;
import com.pusio.budzet.components.OutcomesGrid;
import com.pusio.budzet.entity.Outcomes;
import com.pusio.budzet.entity.OutcomesCategory;
import com.vaadin.data.Binder;
import com.vaadin.data.converter.StringToDoubleConverter;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;

@SuppressWarnings("serial")
public class AddOutcomesView extends AuthenticatedView {

    //PieChartOutcomes pieChartOutcomes;
    Binder<Outcomes> binder = new Binder<>();
    OutcomesGrid outcomesGrid;// = new OutcomesGrid();
    
	public AddOutcomesView() {
		
		this.setSizeFull();
		outcomesGrid = new OutcomesGrid();
		//CREATE COMBO BOX WITH USERS
		
	    ComboBox<OutcomesCategory> OutcomesCatSelect =
	    	    new ComboBox<>("zaznacz Kategorie");
	    OutcomesCatSelect.setItems(new OutcomesCategoryDaoImpl().getAll());
	    OutcomesCatSelect.setItemCaptionGenerator(OutcomesCategory::getName);
	    OutcomesCatSelect.setEmptySelectionAllowed(false);
	    OutcomesCatSelect.setSelectedItem(new OutcomesCategory());
	    		
		//CREATE FORM
		FormLayout formLayout = new FormLayout();
	    TextField amount = new TextField("Kwota");
	    
	    //REGISTER BUTTON
	    Button registerButton = new Button("Register");
	    
	    //ADD COMPONENTS TO LAYOUT
	    formLayout.addComponents(amount, OutcomesCatSelect, registerButton);
	    
        binder.forField(amount)
    	.asRequired("Kwota nie moze byc pusta")
        	.withConverter( new StringToDoubleConverter("Please enter a number"))
        	.bind(Outcomes::getAmount, Outcomes::setAmount);

        binder.forField(OutcomesCatSelect)
    		.asRequired("Kategoria musi byc wybrana")
    		.bind(Outcomes::getCategory, Outcomes::setCategory);
    
        
        Label validationStatus = new Label();
        binder.setStatusLabel(validationStatus);
       	binder.setBean(new Outcomes(
        			0.0, 
        			LocalDateTime.now(),
        			null,  
        			OutcomesCatSelect.getSelectedItem().get())
        			);
        binder.addStatusChangeListener(
                event -> registerButton.setEnabled(binder.isValid()));
        
        formLayout.addComponent(validationStatus);
        registerButton.setEnabled(false);
        registerButton.addClickListener(
                event -> {
                	addTransaction(binder.getBean());
                });
        
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponents(formLayout, outcomesGrid );

        addComponent(horizontalLayout);
        
	}

	private void addTransaction(Outcomes bean) {
		// TODO Auto-generated method stub
		
		bean.setOwner(getActualUser());
		new OutcomesDaoImpl().create(bean);
		
		binder.setBean(new Outcomes());
		outcomesGrid.updateGrid();
	}
}
