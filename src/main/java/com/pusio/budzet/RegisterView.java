package com.pusio.budzet;

import java.util.Objects;


import com.pusio.budzet.DAOimpl.UserDaoImpl;
import com.pusio.budzet.entity.Panda;
import com.vaadin.data.Binder;
import com.vaadin.data.Validator;
import com.vaadin.navigator.View;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class RegisterView extends VerticalLayout implements View {
	
	public RegisterView() {
		// TODO Auto-generated constructor stub
		
	    FormLayout layout = new FormLayout();
	    
	    TextField login = new TextField("Login");
	    TextField email = new TextField("e-mail");
	    PasswordField pass = new PasswordField("Password");
	    PasswordField passRepeat = new PasswordField("Repeat Password");
	    Button registerButton = new Button("Register");

	    
	    layout.addComponents(login, email, pass, passRepeat, registerButton);
	    
        Binder<Panda> binder = new Binder<>();
   	 
        binder.forField(login)
                .asRequired("Name may not be empty")
                .bind(Panda::getLogin, Panda::setLogin);
 
        binder.forField(email)
                .asRequired("Email may not be empty")
                .bind(Panda::getEmail, Panda::setEmail);
 
        binder.forField(pass)
                .asRequired("Password may not be empty")
                .bind(Panda::getPassword, Panda::setPassword);
 
        binder.forField(passRepeat)
                .asRequired("Must confirm password")
                .bind(Panda::getPassword, (person, password) -> {});
 
        binder.withValidator(Validator.from(User -> {
            if (pass.isEmpty() || passRepeat.isEmpty()) {
                return true;
            } else {
                return Objects.equals(pass.getValue(), passRepeat.getValue());
            }
        }, "Entered password and confirmation password must match"));
 
        
        Label validationStatus = new Label();
        binder.setStatusLabel(validationStatus);
        binder.setBean(new Panda(login.getValue(), pass.getValue(), email.getValue() ));
        
        binder.addStatusChangeListener(
                event -> registerButton.setEnabled(binder.isValid()));
        
        layout.addComponent(validationStatus);
        registerButton.setEnabled(false);
        registerButton.addClickListener(
                event -> {
                	registerNewUser(binder.getBean());
                });
 
        addComponent(layout);
	}

	private Object registerNewUser(Panda bean) {
		UserDaoImpl userDaoImpl = new UserDaoImpl();
		userDaoImpl.create(bean);
		
		getUI().getNavigator().navigateTo("");
		return null;
	}
}
