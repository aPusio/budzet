package com.pusio.budzet;

import com.pusio.budzet.DAOimpl.UserDaoImpl;
import com.pusio.budzet.entity.Panda;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;



@SuppressWarnings("serial")
public class AllUsersView extends VerticalLayout implements View {

	Grid<Panda> gridOfUsers;
	
	public AllUsersView() {

		UserDaoImpl users = new UserDaoImpl();
		java.util.List<Panda> hmm = users.getAll();
		
		
		gridOfUsers = new Grid<Panda>();
		gridOfUsers.setItems(hmm);
		gridOfUsers.addColumn(Panda::getLogin).setCaption("LOGIN");
		gridOfUsers.addColumn(Panda::getEmail).setCaption("EMAIL");
		addComponent(gridOfUsers);

	}
	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
			//Notification.show("LOGED USER: " + VaadinSession.getCurrent().getAttribute("user"));
			
			UserDaoImpl users = new UserDaoImpl();
			java.util.List<Panda> hmm = users.getAll();
			
			gridOfUsers.setItems(hmm);
	}
	


}
