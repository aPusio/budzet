package com.pusio.budzet;

import java.time.LocalDateTime;

import com.pusio.budzet.DAOimpl.IncomesCategoryDaoImpl;
import com.pusio.budzet.DAOimpl.IncomesDaoImpl;
import com.pusio.budzet.components.IncomesGrid;
import com.pusio.budzet.entity.Incomes;
import com.pusio.budzet.entity.IncomesCategory;
import com.vaadin.data.Binder;
import com.vaadin.data.converter.StringToDoubleConverter;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;

@SuppressWarnings("serial")
public class AddIncomesView extends AuthenticatedView {
	IncomesGrid incomesGrid = new IncomesGrid();

	private Binder<Incomes> binder = new Binder<>();
	ComboBox<IncomesCategory> incomesCatSelect;
	
	public AddIncomesView() {
		incomesCatSelect = new ComboBox<>("zaznacz Kategorie");
		incomesCatSelect.setItems(new IncomesCategoryDaoImpl().getAll());
		incomesCatSelect.setItemCaptionGenerator(IncomesCategory::getName);
		incomesCatSelect.setEmptySelectionAllowed(false);
		incomesCatSelect.setSelectedItem(new IncomesCategory());

		// CREATE FORM
		FormLayout formLayout = new FormLayout();
		TextField amount = new TextField("Kwota");

		// REGISTER BUTTON
		Button registerButton = new Button("Register");

		// ADD COMPONENTS TO LAYOUT
		formLayout.addComponents(amount, incomesCatSelect, registerButton);

		binder.forField(amount).asRequired("Kwota nie moze byc pusta")
				.withConverter(new StringToDoubleConverter("Please enter a number"))
				.bind(Incomes::getAmount, Incomes::setAmount);

		binder.forField(incomesCatSelect).asRequired("Kategoria musi byc wybrana").bind(Incomes::getCategory,
				Incomes::setCategory);

		Label validationStatus = new Label();
		binder.setStatusLabel(validationStatus);
		
		binder.setBean(new Incomes(0.0, LocalDateTime.now(), null, incomesCatSelect.getSelectedItem().get()));
		
		binder.addStatusChangeListener(event -> registerButton.setEnabled(binder.isValid()));

		formLayout.addComponent(validationStatus);
		registerButton.setEnabled(false);
		registerButton.addClickListener(event -> {
			addTransaction(binder.getBean());
		});

		HorizontalLayout horizontalLayout = new HorizontalLayout();
		horizontalLayout.addComponents(formLayout, incomesGrid);
		addComponent(horizontalLayout);

	}

	private void addTransaction(Incomes bean) {
		bean.setOwner(getActualUser());
		
		new IncomesDaoImpl().create(bean);
		binder.setBean(new Incomes());

		incomesGrid.updateGrid();
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		super.enter(event);
	}
}
