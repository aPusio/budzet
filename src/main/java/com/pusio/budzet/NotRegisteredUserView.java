package com.pusio.budzet;

import com.vaadin.annotations.Theme;
import com.vaadin.navigator.View;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;


@SuppressWarnings("serial")
@Theme("Valo")
public class NotRegisteredUserView extends VerticalLayout implements View{
	//String targetView;
	
	public NotRegisteredUserView() {
		VerticalLayout layout = new VerticalLayout();
		
		layout.setPrimaryStyleName(ValoTheme.LABEL_H4);
		layout.addComponent(new Label("ZAREJESTRUJ SIE"));
		
		
		addComponent(layout);
		
	}
}
