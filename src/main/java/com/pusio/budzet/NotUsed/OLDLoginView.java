package com.pusio.budzet.NotUsed;

import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;

public class OLDLoginView extends CustomComponent {
	 
	  private TextField login = new TextField("Login");	 
	  private TextField password = new TextField("Password");
	  
	  public OLDLoginView() {
	 
	    FormLayout layout = new FormLayout();
	 
	    setCompositionRoot(layout);
	 
	    layout.addComponent(login);
	    layout.addComponent(password);
	 
	    Button button = new Button("Login");
	 
	    layout.addComponent(button);
	  }
	}