package com.pusio.budzet.NotUsed;

import java.util.Objects;

import javax.servlet.annotation.WebServlet;

import com.pusio.budzet.entity.Panda;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.data.Binder;
import com.vaadin.data.Validator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

public class FormUI extends UI {
 	
	private VerticalLayout vlayout = new VerticalLayout();
	
	@Override
	protected void init(VaadinRequest request) {
		// TODO Auto-generated method stub
		  	
			FormLayout layout = new FormLayout();
			vlayout.addComponents(layout, new Button("ButtonFormForm"));
			
		
			layout = new FormLayout();
	        layout.setSpacing(false);
	        layout.setMargin(false);
	        layout.setCaption("Registration Form");
	 
	       // layout.addComponent("ButtonFromForm");
	        
	        TextField fullNameField = new TextField("Login");
	        TextField emailField = new TextField("Email");
	        PasswordField passwordField = new PasswordField("Password");
	        PasswordField confirmPasswordField = new PasswordField("Confirm Password");
	 
	        Binder<Panda> binder = new Binder<>();
	 
	        binder.forField(fullNameField)
	                .asRequired("Name may not be empty")
	                .bind(Panda::getLogin, Panda::setLogin);
	 
	        binder.forField(emailField)
	                .asRequired("Email may not be empty")
	                //.withValidator(new EmailValidator("Not a valid email address"))
	                .bind(Panda::getEmail, Panda::setEmail);
	 
	        binder.forField(passwordField)
	                .asRequired("Password may not be empty")
	               // .withValidator(new StringLengthValidator(
	               //         "Password must be at least 7 characters long", 7, null))
	                .bind(Panda::getPassword, Panda::setPassword);
	 
	        binder.forField(confirmPasswordField)
	                .asRequired("Must confirm password")
	                .bind(Panda::getPassword, (person, password) -> {});
	 
	        binder.withValidator(Validator.from(User -> {
	            if (passwordField.isEmpty() || confirmPasswordField.isEmpty()) {
	                return true;
	            } else {
	                return Objects.equals(passwordField.getValue(),
	                        confirmPasswordField.getValue());
	            }
	        }, "Entered password and confirmation password must match"));
	 
	        Label validationStatus = new Label();
	        binder.setStatusLabel(validationStatus);
	 
	        binder.setBean(new Panda());
	 
	        Button registerButton = new Button("Register");
	        registerButton.setEnabled(false);
	        registerButton.addClickListener(
	                event -> registerNewUser(binder.getBean()));
	 
	        binder.addStatusChangeListener(
	                event -> registerButton.setEnabled(binder.isValid()));
	        
	        
	        setContent(vlayout);
	}

	private Object registerNewUser(Panda bean) {
		// TODO Auto-generated method stub
		
		
		return null;
	}

	public VerticalLayout getVlayout() {
		return vlayout;
	}

	public void setVlayout(VerticalLayout vlayout) {
		this.vlayout = vlayout;
	}	
	
//    @WebServlet(urlPatterns = "/form", name = "MyUIServlet", asyncSupported = true)
//    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
//    public static class MyUIServlet extends VaadinServlet {
//    }
}


