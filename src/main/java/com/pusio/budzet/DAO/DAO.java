package com.pusio.budzet.DAO;

import java.util.List;

public interface DAO<T> {

	void create(T entity);
	void delete(T entity);
	List<T> getAll();
	T getById(Long id);
	void update(T entity);
}

